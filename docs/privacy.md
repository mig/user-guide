# Privacy Notice

The messaging service privacy notice can be found in the
[CERN Service Portal](https://cern.service-now.com/service-portal?id=privacy_policy&se=Messaging-Service&notice=main).
