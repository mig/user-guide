# Glossary

Here are brief explanations of the most important messaging related terms.

## ActiveMQ

[ActiveMQ](https://activemq.apache.org/components/classic/) (now known as "ActiveMQ Classic")
is an open source messaging software. From its website:

> Apache ActiveMQ Classic is a popular and powerful open source messaging and Integration Patterns server.

Despite the addition of [Artemis](glossary.md#artemis) under the ActiveMQ
"umbrella", the old classic software is still being developed. The latest
major version is 6, released at the end of 2023. See its
[download page](https://activemq.apache.org/components/classic/download/)
for more information.

## AMQP

[AMQP](https://en.wikipedia.org/wiki/Advanced_Message_Queuing_Protocol)
(meaning *Advanced Message Queuing Protocol*)
is a messaging protocol.

In fact, there are two very different major versions of AMQP:

* 0-x (i.e. 0-8, 0-9, 0-10...) used mainly by [RabbitMQ](glossary.md#rabbitmq)
* 1.0 (officially "ISO/IEC 19464") supported by all major messaging software

## Artemis

[Artemis](https://activemq.apache.org/components/artemis/) (now known as "ActiveMQ Artemis")
is an open source messaging software. From its website:

> The Next Generation Message Broker by ActiveMQ

In fact, it is a descendant of [HornetQ](https://en.wikipedia.org/wiki/HornetQ)
and it is being developed alongside [ActiveMQ Classic](glossary.md#activemq).

## Body

See [message](glossary.md#message) body.

## Broker

A [message broker](https://en.wikipedia.org/wiki/Message_broker)
is an intermediary program that receives messages from senders
and forwards them to receivers. It allows for asynchronous message delivery.

Most messaging software use brokers, with the exception of [ZeroMQ](glossary.md#zeromq).

## Header

See [message](glossary.md#message) header.

## JMS

[JMS](https://en.wikipedia.org/wiki/Java_Message_Service)
(meaning *Java Message Service*)
is a messaging API, not a wire protocol like [AMQP](glossary.md#amqp).

It is widely used and its main models
([point-to-point](glossary.md#point-to-point) and [publish/subscribe](glossary.md#publishsubscribe))
are supported by all major messaging software.

## Message

A **message**
(as used in [Message Oriented Middleware](https://en.wikipedia.org/wiki/Message-oriented_middleware))
consists of a header and a body.

The **header** is a collection of header fields.

Each **header field** is a key/value pair where the key and the value are
text strings. The key is unique within the header so we can use a hash table
(Perl) or dictionary (Python) to represent the header of the message.

The **body** is either a text string or a binary string. This distinction is
needed because text may need to be encoded (for instance using UTF-8) before
being stored on disk or sent across the network.

When putting data into a message, the data itself is often put in the body
while information about the data (also known as meta-data) ends up in the
header.

## Messaging

From *[The Java EE Tutorial](https://javaee.github.io/tutorial/toc.html)*:

> Messaging is a method of communication between software components or
> applications. A messaging system is a peer-to-peer facility: A messaging
> client can send messages to, and receive messages from, any other client. Each
> client connects to a messaging agent that provides facilities for creating,
> sending, receiving, and reading messages.
> 
> Messaging enables distributed communication that is loosely coupled. A
> component sends a message to a destination, and the recipient can retrieve the
> message from the destination. What makes the communication loosely coupled is
> that the destination is all that the sender and receiver have in common. The
> sender and the receiver do not have to be available at the same time in order
> to communicate. In fact, the sender does not need to know anything about the
> receiver; nor does the receiver need to know anything about the sender. The
> sender and the receiver need to know only which message format and which
> destination to use.

## MQTT

[MQTT](https://en.wikipedia.org/wiki/MQTT)
(meaning *Message Queuing Telemetry Transport*)
is a messaging protocol targeted at [IoT](https://en.wikipedia.org/wiki/Internet_of_things) devices.

## OpenWire

[OpenWire](https://activemq.apache.org/components/classic/documentation/openwire)
is a messaging protocol used only by [ActiveMQ](glossary.md#activemq).

## Point-to-point

From *[The Java EE Tutorial](https://javaee.github.io/tutorial/toc.html)*:

> A point-to-point (PTP) product or application is built on the concept of
> message queues, senders, and receivers. Each message is addressed to a
> specific queue, and receiving clients extract messages from the queues
> established to hold their messages. Queues retain all messages sent to them
> until the messages are consumed or expire.
> 
> PTP messaging, has the following characteristics:
> 
> * Each message has only one consumer.
> * The receiver can fetch the message whether or not it was running when the
>   client sent the message.
> 
> Use PTP messaging when every message you send must be processed successfully
> by one consumer.

<p style="text-align:center"><img src="img/queue.png" alt="JMS queue"></p>

See also Wikipedia's *[message queue](https://en.wikipedia.org/wiki/Message_queue)*.

## Publish/subscribe

From *[The Java EE Tutorial](https://javaee.github.io/tutorial/toc.html)*:

> In a publish/subscribe (pub/sub) product or application, clients address
> messages to a topic, which functions somewhat like a bulletin board.
> Publishers and subscribers can dynamically publish or subscribe to the
> topic. The system takes care of distributing the messages arriving from a
> topic’s multiple publishers to its multiple subscribers. Topics retain
> messages only as long as it takes to distribute them to subscribers.
> 
> Pub/sub messaging has the following characteristics:
> 
> * Each message can have multiple consumers.
> * A client that subscribes to a topic can consume only messages sent after the
>   client has created a subscription, and the consumer must continue to be
>   active in order for it to consume messages.
> 
> Use pub/sub messaging when each message can be processed by any number of
> consumers (or none).

<p style="text-align:center"><img src="img/topic.png" alt="JMS topic"></p>

See also Wikipedia's *[publish–subscribe pattern](https://en.wikipedia.org/wiki/Publish%E2%80%93subscribe_pattern)*.

## Queue

A message queue implements [JMS](glossary.md#jms)'
[point-to-point model](glossary.md#point-to-point).

## RabbitMQ

[RabbitMQ](https://www.rabbitmq.com/) is an open source messaging software.
From its website:

> RabbitMQ is a reliable and mature messaging and streaming broker, which is
> easy to deploy on cloud environments, on-premises, and on your local
> machine. It is currently used by millions worldwide.

## STOMP

[STOMP](stomp.md)
(meaning *Streaming Text Oriented Messaging Protocol*)
is a lightweight messaging protocol.

## Topic

A message topic implements [JMS](glossary.md#jms)'
[publish/subscribe model](glossary.md#publishsubscribe).

## Virtual Destinations

[ActiveMQ](glossary.md#activemq)'s
[virtual destinations](https://activemq.apache.org/components/classic/documentation/virtual-destinations)
allow to mix the concepts of [queues](glossary.md#queue) and [topics](glossary.md#topic).

Here is how they are used at CERN.

Producers simply see a topic and send messages to a destination like `/topic/foo`.

One ore more virtual queues are attached to it, for instance a `test` queue and a `prod` queue,
corresponding to destinations like `/queue/Consumer.test.foo` and `/queue/Consumer.prod.foo`.

Consumers see two different queues and simply consume from any of them, like any other queue.

Each incoming message is duplicated and ends up in both queues.

## WebSocket

[WebSocket](https://en.wikipedia.org/wiki/WebSocket)
allows web applications to maintain bidirectional communications with server-side processes.

It is not a messaging protocol but it can wrap protocols like [STOMP](glossary.md#stomp)
so that they can be used from a web browser.

See for instance [ActiveMQ](glossary.md#activemq)'s
[WebSockets page](https://activemq.apache.org/websockets)
for more practical information.

## ZeroMQ

[ZeroMQ](https://zeromq.org/) (aka 0MQ) is a brokerless open source messaging software.
From [Wikipedia](https://en.wikipedia.org/wiki/ZeroMQ):

> ZeroMQ is a high-performance asynchronous messaging library, aimed at use in
> distributed or concurrent applications. It provides a message queue, but
> unlike message-oriented middleware, a ZeroMQ system can run without a
> dedicated message broker.
