# Contact

The recommended way to contact the CERN Messaging Team is to use ServiceNow to
report an incident or submit a request. This can be done at <https://cern.ch/mig/support>.

Alternatively, the team can also be reached by email at <code>mig&#64;cern&#46;ch</code>.
