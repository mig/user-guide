# Service Description

## Overview

[CERN's IT Department](https://cern.ch/information-technology) provides
different messaging services dedicated to groups of related messaging clients.

A dedicated messaging service typically contains two distinct (sub)services:

* a test service consisting of a single test broker
* a production service consisting of two or more production brokers

The brokers currently run ActiveMQ but this might change in the future.

The recommended protocol to interact with the brokers is [STOMP](stomp.md).

## Port Numbers

Each messaging service uses a fixed set of port numbers. Usually, matching
test and production services use the same set of port numbers.

Each port number maps to a dedicated protocol, typically:

* STOMP: 6*xy*13
* STOMP+SSL: 6*xy*23
* STOMP+WebSocket: 6*xy*33

## Security

Authentication is required to access the brokers. Two mechanisms are
supported:

* name/password authentication
* X.509 authentication (warning: proxy certificates are **not** supported)

The user is responsible for making sure that the credentials they use are
adequately protected.

For integrity and reliability purposes messaging services are monitored. This
includes logging of network connections and commands. In addition, when
required, messages exchanged between clients might be intercepted, monitored
or inspected.

## Test Services

A test messaging service usually consists of a single broker.

The goal is to test changes before propagating them to production so test
brokers run on the same kind of hardware and are monitored exactly like
production brokers.

Test brokers can be stopped at any time without any announcement in order to
perform maintenance operations such as:

* operating system upgrades and reboots
* messaging software upgrades and configuration changes

## Production Services

A production messaging service usually consists of two or more brokers.

Except in emergency situations (such as a security hole needing to be fixed),
significant changes on production brokers are first carefully tested on the
matching test brokers before being applied.

Individual production brokers may be stopped at any time (for instance for an
operating system maintenance operation) but, thanks to Load Balanced DNS, the
overall availability of the service is always preserved.

### Operating System Changes

Significant operating system changes (such as a new kernel or a new version of
Java) are normally tested one month before being applied to production brokers.

Minor changes or emergency changes might be deployed more quickly.

In any case, since these changes are transparent to the messaging services,
they are not announced beforehand.

### Messaging Software Changes

Significant messaging software changes (such as a new major software version)
are normally tested one month before being applied to production brokers.

All significant messaging software changes are announced beforehand to the
messaging users so that they can further test the upcoming changes (on their
test brokers) and stop or delay the change in case serious problems are found.

Minor changes or emergency changes might be deployed more quickly.

### Broker Unavailability

In case a broker has a planned future downtime (announced or not), it will be
removed from the corresponding DNS alias(es) at least one hour prior to the
downtime. It will be added back only after successful functionality testing.

This guarantees that, during normal operations, all the brokers behind a DNS
alias are fully working.

Of course, unplanned downtime (such as hardware failure, power cut...) can
happen. In this case too, brokers will be removed from DNS. This removal is
automated and provided by the Load Balanced DNS service.

## Monitoring

All the messaging services (both test and production) are
[monitored](monitoring.md).

Most of the alarms are forwarded to the messaging service managers but some
are sent to messaging users.
