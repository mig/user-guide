# stompclt

`stompclt` is a versatile tool to interact with messaging brokers speaking
[STOMP](stomp.md) and/or message queues
(see [Messaging::Message::Queue](https://metacpan.org/pod/Messaging::Message::Queue))
on disk.

It receives messages (see [Messaging::Message](https://metacpan.org/pod/Messaging::Message))
from an incoming module, optionally massaging them (i.e. filtering and/or modifying),
and sends them to an outgoing module. Depending on which modules are used,
the tool can perform different operations.

Here are the supported incoming modules:

* broker:
  connect to a messaging broker using STOMP, subscribe to one or more
  destinations and receive the messages sent by the broker
* queue:
  read messages from a message queue on disk

Here are the supported outgoing modules:

* broker:
  connect to a messaging broker using STOMP and send the messages
* queue:
  store the messages in a message queue on disk

Here are some frequently used combinations:

* *incoming broker* + *outgoing queue*:
  drain some destinations, storing the messages on disk
* *incoming queue* + *outgoing broker*:
  (re-)send messages that have been previously stored on disk, optionally
  with modifications (such as altering the destination)
* *incoming broker* + *outgoing broker*:
  shovel messages from one broker to another

More information can be found in its
[man page](https://cern-mig.github.io/stompclt/).

Its sources are in GitHub: [cern-mig/stompclt](https://github.com/cern-mig/stompclt).

Ready-to-use packages are available in the
[EPEL](https://fedoraproject.org/wiki/EPEL) and
[Fedora](https://docs.fedoraproject.org/en-US/quick-docs/repositories/) repositories.
