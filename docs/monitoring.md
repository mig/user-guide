# Monitoring

The message brokers are monitored using the tools provided by [MONIT](https://cern.ch/monit).

[Grafana](https://grafana.com) based dashboards are available at <https://cern.ch/mig/grafana>.

[Kapacitor](https://www.influxdata.com/time-series-platform/kapacitor/)
is used to raise alarms in case of problems.

The most common problem is unconsumed messages on the brokers but other
problems (like too few or too many messages received) can also be detected.
