# CERN Messaging Service - User Guide

This guide describes how to *use* the
[CERN Messaging Service](https://cern.ch/mig) (aka MIG).

Its official URL is <https://mig-user.docs.cern.ch>.
