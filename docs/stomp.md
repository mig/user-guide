# STOMP

STOMP stands for *Streaming Text Orientated Messaging Protocol* or *Simple
Text Oriented Messaging Protocol*.

STOMP provides an interoperable wire format so that STOMP clients can
communicate with any STOMP message broker to provide easy and widespread
messaging interoperability among many languages, platforms and brokers.

It is the recommended protocol to interact with the message brokers at CERN.

For message clients, we recommend the following libraries:

* Perl: [Net::STOMP::Client](https://metacpan.org/pod/Net::STOMP::Client)
* Python: [stomp.py](https://github.com/jasonrbriggs/stomp.py)

References:

* [STOMP home page](https://stomp.github.io/)
* [latest protocol specification](https://stomp.github.io/stomp-specification-1.2.html)
