# Puppet

When using Puppet to manage messaging clients, one can take advantage of the
`query_nodes()` function to get the list of brokers which are part of a cluster.

For instance, for the `foo` cluster, the following manifest snippet will return
the list of broker host names:

```
$brokers = unique(query_nodes('hostgroup_0="mig" and hostgroup_1="brokers" and hostgroup_2="foo"', 'hostname'))
```

From the command line, the `ai-pdb` tool can be used:

```
% ai-pdb hostgroup mig/brokers/foo
```
